﻿using System.Web;
using System.Web.Optimization;

namespace Calendar
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                      "~/Scripts/angular.js",
                      "~/Scripts/angular-ui-router.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/angular-app").Include(
                      "~/Scripts/app/module.app.js",
                      "~/Scripts/app/route.app.js",
                      "~/Scripts/app/pages/calendar/ctrl.calendar.js",
                      "~/Scripts/app/pages/calendar/srv.calendar.js",
                      "~/Scripts/app/pages/calendar/dir.eventDetail.js"
            ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
