﻿using Calendar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Calendar.Controllers
{
    public class CalendarController : ApiController
    {
        private readonly IEventService _eventService;

        public CalendarController(IEventService eventService) 
        {
            this._eventService = eventService;
        }

        [Route("api/calendar/events")]
        public IEnumerable<Event> GetAllEvents()
        {
            return _eventService.GetAllEvents();
        }

        [Route("api/calendar/events/{monthIndex}")]
        public IEnumerable<Event> GetAllEventsByMonthIndex(int monthIndex)
        {
            return _eventService.GetAllEventsByMonthIndex(monthIndex);
        }
    }
}
