(function () {
    'use strict';

    angular
        .module('calendar-app')
        .factory('calendarService', calendarService);

    calendarService.$inject = ['$http', '$q'];
    function calendarService($http, $q) {
        var service = {
            getAllEventsByMonthIndex: getAllEventsByMonthIndex,
            getAllEvents: getAllEvents,
            getMonthNames: getMonthNames
        };

        return service;

        ////////////////
        function getAllEventsByMonthIndex(monthIndex) {
            return $http.get('api/calendar/events/' + (++monthIndex)).then(function (response) {
                return response.data;
            });
        }

        function getAllEvents() {
            return $http.get('api/calendar/events').then(function (response) {
                return response.data;
            });
        }

        function getMonthNames() {
            return ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        }
    }
})();