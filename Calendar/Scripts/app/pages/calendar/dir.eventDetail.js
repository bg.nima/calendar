(function() {
    'use strict';

    angular
        .module('calendar-app')
        .directive('eventDetail', eventDetail);

    eventDetail.$inject = [];
    function eventDetail() {
        var directive = {
            link: link,
            restrict: 'EA',
            templateUrl: 'Scripts/app/pages/calendar/view.eventDetail.html',
            scope: {
                event:'='
            }
        };
        return directive;
        
        function link(scope, element, attrs) {
        }
    }
})();