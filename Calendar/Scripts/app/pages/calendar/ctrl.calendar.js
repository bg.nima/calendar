(function() {
    'use strict';

    angular
        .module('calendar-app')
        .controller('CalendarController', CalendarController);

    CalendarController.$inject = ['calendarService'];
    function CalendarController(calendarService) {
        var vm = this;
        vm.months = [];
        vm.selectMonth = selectMonth;
        vm.selectedMonth = null;
        vm.selectedEvent = null;
        vm.selectEvent = selectEvent;
        vm.events = [];
        
        activate();

        ////////////////

        function activate() {
            vm.months = calendarService.getMonthNames();
            vm.selectedMonth = vm.months[0];
            selectMonth(vm.selectedMonth);
         }

         function selectEvent(ev) {
             vm.selectedEvent = ev;
         }

        function selectMonth(monthName) {
            var index = vm.months.indexOf(monthName);
            calendarService.getAllEventsByMonthIndex(index).then(function (events) {
                vm.events = events;
                selectEvent(null);
            });
        }
    }
})();