﻿(function () {
    "use strict";

    angular.module('calendar-app', [
        'ui.router'
    ]);
})();