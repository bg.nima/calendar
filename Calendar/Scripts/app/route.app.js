﻿(function () {
    'use strict';

    angular
        .module('calendar-app')
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

            // For any unmatched url, redirect to /dashboard
            $urlRouterProvider.otherwise("/calendar");

            $stateProvider
                .state('app', {
                    url: '',
                    templateUrl: 'Scripts/app/view.root.html',
                    //controller: 'RootController',
                    abstract: true
                })
                .state('app.calendar', {
                    url: '/calendar',
                    templateUrl: 'Scripts/app/pages/calendar/view.calendar.html',
                    controller: 'CalendarController',
                    controllerAs: 'vm'
                });

        }]);
})();
