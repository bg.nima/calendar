﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Calendar.Models
{
    public class EventService : IEventService
    {
        public IEnumerable<Event> GetAllEvents()
        {
            var path = System.Web.Hosting.HostingEnvironment.MapPath("~/MOCK_DATA.json");
            using (StreamReader file = File.OpenText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                Event[] events = (Event[])serializer.Deserialize(file, typeof(Event[]));
                return events;
            }
        }

        public IEnumerable<Event> GetAllEventsByMonthIndex(int monthIndex)
        {
            return GetAllEvents().Where(e => e.Date.Month == monthIndex);
        }
    }
}