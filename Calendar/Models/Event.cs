﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calendar.Models
{
    public class Event
    {
        public Event()
        {
            Attendees = new List<string>();
        }

        public Event(string desc, string organizer, DateTime date, IEnumerable<string> attendees): this()
        {
            this.Description = desc;
            this.Organizer = organizer;
            this.Date = date;
            this.Attendees.AddRange(attendees);
        }

        public string Description { get; set; }
        public string Organizer { get; set; }
        public DateTime Date { get; set; }
        public List<string> Attendees { get; set; }
    }
}