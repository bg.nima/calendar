crossvertise GmbH
=================

Simple single page app using angularjs. all UI and .Net dependencies are provided through Nuget.
I used StructureMap as IoC container and MOQ for mocking my services.

angularjs application files are located in "~/Scripts/app" folder.

