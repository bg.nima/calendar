﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Calendar.Models;
using System.Collections.Generic;
using Calendar.Controllers;
using System.Linq;

namespace Calendar.Tests.Controllers
{
    [TestClass]
    public class CalendarControllerTest
    {
        [TestMethod]
        public void GetAllEvents()
        {
            IEventService eventService = new EventServiceMock().GetEventServiceInstance();
            CalendarController calendarController = new CalendarController(eventService);

            var result = calendarController.GetAllEvents();
            
            Assert.AreEqual(4, result.Count());
            Assert.AreEqual("event 1", result.First().Description);
        }

        [TestMethod]
        public void GetAllEventsByMonthIndex_ThisMonth()
        {
            IEventService eventService = new EventServiceMock().GetEventServiceInstance();
            CalendarController calendarController = new CalendarController(eventService);

            var result = calendarController.GetAllEventsByMonthIndex(DateTime.Now.Month);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("event 1", result.First().Description);
        }

    }
}
