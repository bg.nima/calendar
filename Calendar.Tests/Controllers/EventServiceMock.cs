﻿using Calendar.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calendar.Tests
{
    public class EventServiceMock
    {
        private Mock<IEventService> mock;
        public EventServiceMock()
        {
            var events = new List<Event>();
            events.Add(new Event("event 1", "Nima", DateTime.Now, new string[] { "Jack", "Jim" }));
            events.Add(new Event("event 2", "Jack", DateTime.Now.AddMonths(-1), new string[] { "Sara", "Noma" }));
            events.Add(new Event("event 3", "Jim", DateTime.Now.AddMonths(-2), new string[] { "Jack", "Sara" }));
            events.Add(new Event("event 4", "Sara", DateTime.Now.AddMonths(-3), new string[] { "Nima", "Jim" }));
            mock = new Mock<IEventService>();
            mock.Setup(es => es.GetAllEvents()).Returns(events);
            mock.Setup(es => es.GetAllEventsByMonthIndex(DateTime.Now.Month)).Returns(events.Where(e => e.Date.Month == DateTime.Now.Month));
        }

        public IEventService GetEventServiceInstance()
        {
            return mock.Object;
        }
    }
}
